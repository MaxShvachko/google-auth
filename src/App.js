import React, {Component} from 'react';
import './main.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      image: '',
    };

    this.signIn = this.signIn.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  componentDidMount() {
    window.gapi.load('auth2', () => {
      window.gapi.auth2.init({
        client_id: '764558934903-fko7aacbl9b5d1uftuq7nagmro2sadge.apps.googleusercontent.com',
      })
        .then(() => console.log('init was ok'))
        .catch(err => console.log(err))
    })
  }

  signIn() {
    const addUserName = user => {
      const userName = user.getBasicProfile().getName();
      this.setState(prevState => ({name: userName}))
    };

    const addUserImage = user => {
      const imageUrl = user.getBasicProfile().getImageUrl();
      this.setState(prevState => ({image: imageUrl}))
    };

    const GoogleAuth = window.gapi.auth2.getAuthInstance();
    GoogleAuth.signIn()
      .then(user => {
        addUserName(user);
        addUserImage(user);
      })

      .catch(err => console.log(err))
  }

  signOut() {
    const GoogleAuth = window.gapi.auth2.getAuthInstance();
    GoogleAuth.signOut()
      .then(this.setState(prevState => (
        {
          name: '',
          image: '',
        })
      ))
      .catch(err => console.log(err))
  };

  render() {
    const {name, image} = this.state;
    return (
      <div className="wrapper">
        <header className="header">
          <h2 className='header__title'>LogIn with google</h2>
        </header>
        <main className='main'>
          <div className='main__content'>
            <div className='auth-info'>
              {!name ?
                <h3 className='auth-info__title'>Push the button LogIn</h3>
                :
                <React.Fragment>
                  <img className='auth-info__image' src={image} alt="Avatar"/>
                  <p className='auth-info__text'>{name}</p>
                </React.Fragment>
              }
            </div>
          </div>
          {!name ?
            <div className='btn-container'>
              <button className='btn-container__item' onClick={this.signIn}>LogIn</button>
            </div>
            :
            <div className='btn-container'>
              <button className='btn-container__item btn-container__item--signOut' onClick={this.signOut}>LogOut
              </button>
            </div>
          }
        </main>
      </div>
    );
  }
}

export default App;
